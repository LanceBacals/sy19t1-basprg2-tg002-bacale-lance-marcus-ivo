#include <iostream>
#include <vector>
#include <time.h>
using namespace std;


void comparing(string playerCard, string dealerCard, string& winner) {
	if (playerCard == dealerCard) {
		cout << "Open!" << endl << endl;
		cout << "[Ryujin] " << playerCard << " v " << "[Yeji] " << dealerCard << endl << endl;
		cout << "Draw!" << endl;
		system("pause");
		system("CLS");
		winner = "d";
	}
	else if (playerCard == "Citizen" && dealerCard == "Slave") {
		cout << "Open!" << endl << endl;
		cout << "[Ryujin] " << playerCard << " v " << "[Yeji] " << dealerCard << endl << endl;	
		cout << "Ryujin wins!" << endl;
		system("pause");
		system("CLS");
		winner = "w";
	}
	else if (playerCard == "Slave" && dealerCard == "Emperor") {
		cout << "Open!" << endl << endl;
		cout << "[Ryujin] " << playerCard << " v " << "[Yeji] " << dealerCard << endl << endl;
		cout << "Ryujin wins!" << endl;
		system("pause");
		system("CLS");
		winner = "s";
	}
	else if (playerCard == "Emperor" && dealerCard == "Citizen") {
		cout << "Open!" << endl << endl;
		cout << "[Ryujin] " << playerCard << " v " << "[Yeji] " << dealerCard << endl << endl;
		cout << "Ryujin wins!" << endl;
		system("pause");
		system("CLS");
		winner = "w";
	}
	else {
		cout << "Open!" << endl << endl;
		cout << "[Ryujin] " << playerCard << " v " << "[Yeji] " << dealerCard << endl << endl;
		cout << "Yeji wins!" << endl;
		cout << "*Drill sound effects*" << endl;
		cout << "Ryujin: YAMETEEEEEEEEEEEEEEEEEEEEEE!!!!!!!!!" << endl;
		system("pause");
		system("CLS");
		winner = "l";
	}	
}

int main()
{
	srand(time(NULL));
	string emperorSlave = "Emperor";
	string dealerSide = "Slave";
	string dealerCard;
	string playerCard;
	string winner;
	int rounds = 1;
	int cash = 0;
	int distance = 30;
	int wager = 0;
	int multiply = 100000;
	do {

		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << distance << endl;
		cout << "Rounds: " << rounds << "/12" << endl;
		cout << "Side: " << emperorSlave << endl << endl;
		cout << "How many mm would you like to wager, Ryujin? ";
		cin >> wager;
		while (wager > distance) {
			cout << "Invalid input! (must not be higher than the distance left!!: ";
			cin >> wager; }
		system("CLS");
		vector<string> card;
		card.push_back(emperorSlave);
		card.push_back("Citizen");
		card.push_back("Citizen");
		card.push_back("Citizen");
		card.push_back("Citizen");

		int counter = 0;
		int cardPick = 0;

		while (counter != 3) {

			cout << "Pick a card" << endl << "=====================" << endl;
			for (unsigned int i = 0; i < card.size(); i++) {
				cout << "[" << i + 1 << "]" << card[i] << endl;
			}
			cin >> cardPick;
			//dealerPicking;
			int cards = 3;
			string dealerCard;
			int pick;
			pick = rand() % cards;

			switch (pick) {
			case 0:
				dealerCard = dealerSide;
				break;
			case 1:
				dealerCard = "Citizen";
				cards = cards - 1;
				break;
			case 2:
				dealerCard = "Citizen";
				cards = cards - 1;
				break;
			case 3:
				dealerCard = "Citizen";
				cards = cards - 1;
				break;
			case 4:
				dealerCard = "Citizen";
				cards = cards - 1;
				break;
			default: cout << "Invalid";
			}
			//PlayerPicking
			switch (cardPick) {
			case 1:
				playerCard = emperorSlave;
				break;
			case 2:
				playerCard = "Citizen";
				card.erase(card.begin() + 1);
				break;
			case 3:
				playerCard = "Citizen";
				card.erase(card.begin() + 2);
				break;
			case 4:
				playerCard = "Citizen";
				card.erase(card.begin() + 3);
				break;
			case 5:
				playerCard = "Citizen";
				card.erase(card.begin() + 4);
				break;
			default:
				cout << "invalid";
			}			
			system("CLS");
			// comparing
			comparing(playerCard, dealerCard, winner);
			counter++;
			if (winner == "w" || winner == "l") {
				break;
			}		
		}//rewards or meh
		if (winner == "l") {
			distance = distance - wager;
		}
		else if (winner == "w") {
			multiply = multiply * wager;
			cash = cash + multiply;
		}
		else if (winner == "s") {
			multiply = multiply * wager;
			cash = cash + multiply;
			cash = cash * 5;
		}
		switch (rounds) {
		case 3:
			emperorSlave = "Slave";
			dealerSide = "Emperor";
			break;
		case 6:
			emperorSlave = "Emperor";
			dealerSide = "Slave";
			break;
		case 9:
			emperorSlave = "Slave";
			dealerSide = "Emperor";
			break;
		}
		rounds++;
		if (distance <= 0) {
			cout << "Distance left (mm): " << distance << endl;
			cout << "Rounds: " << rounds << "/12" << endl;
			cout << "Side: " << emperorSlave << endl << endl;
			cout << "DRILL!!!!!!!!" << endl;
			cout << "Ryujin: ......ded..." << endl;
			break;
		}
	} while (rounds != 12);

	if (cash >= 20000000) {
		cout << "Cash: " << cash << endl;
		cout << "DALLA DALLA YEAH!!!!" << endl;
	}
	else if (cash < 20000000) {
		cout << "Cash: " << cash << endl;
		cout << "JYP: You survived..good work..but not enough...." << endl;
	}
	system("pause");
	return 0;
}