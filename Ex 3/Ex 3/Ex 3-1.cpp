#include <iostream>
#include <time.h>

using namespace std;

void arrayGenerator(int* numbers, int size) {
	srand(time(NULL));
	for (int i = 0; i < size; i++) {
		numbers[i] = rand() % 9;
	}
}

int main() {
	const int size = 5;
	int* number[size];

	arrayGenerator( number[5], size);

	cout << number;
	system("pause");
	return 0;
}