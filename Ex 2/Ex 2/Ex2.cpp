#include <iostream>
#include <time.h>
using namespace std;


// Version 1 function

void bet(int& playerBetRef, int& goldRef) {

	goldRef -= playerBetRef;

}
void diceRoll(int& d1, int& d2) {
	d1 = rand() % 6 + 1;
	d2 = rand() % 6 + 1;
}
void evaluator(int playerRoll, int aiRoll, int& pot, int& pGold ) {
	
	if (playerRoll > aiRoll) {
		cout << "Player Wins!" << endl;
		pGold = pGold + pot;
	}
	if (playerRoll < aiRoll) {
		cout << "Ai Wins!" << endl;
		pGold = pGold - pot;
	}
	if (playerRoll == aiRoll) {
		cout << "Draw!" << endl;
	}
	
}
int main() {
		int playerBet; int gold = 1000;
	srand(time(NULL));

	while (gold > 0) {

		system("cls");
		
		cout << "Player's gold: " << gold << endl;
		cout << "Input bet: ";
		cin >> playerBet;

		while (playerBet == 0 && playerBet < gold) {
			cin >> playerBet;
		}

		bet(playerBet, gold);
		// Ex 2-2 dice Roll
		int playerD1, playerD2, aiD1, aiD2;

		diceRoll(playerD1, playerD2);
		diceRoll(aiD1, aiD2);
		cout << "pDie 1: " << playerD1 << endl;
		cout << "pDie 2: " << playerD2 << endl;
		cout << "aDie 1: " << aiD1 << endl;
		cout << "aDie 2: " << aiD2 << endl;

		evaluator(playerD1 + playerD2, aiD1 + aiD2, playerBet, gold);

		system("pause");


	}


	system("pause");
	return 0;
}

