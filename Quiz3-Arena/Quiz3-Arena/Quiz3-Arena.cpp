#include <iostream>
#include <time.h>
#include "Character.h"
using namespace std;
void printStats(string name, string type, int hp, int pow, int vit, int agi, int dex) {
	cout << "Name: " << name << endl;
	cout << "Class: " << type << endl;
	cout << "Hp: " << hp << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Agi: " << agi << endl;
	cout << "Dex:" << dex << endl;

}
void playerAttack(int* ehp, int *pow, int *evit, int bonusDamage, int* damageP) {
	int damage;
	if (bonusDamage == 1) {
		damage = (*pow - *evit);
		damage = damage + (damage * .5);
	}
	else if (bonusDamage == 0) {
		damage = (*pow - *evit);
	}
	
	*ehp = *ehp - damage;
	*damageP = damage;
}
void enemyAttack(int* hp, int* epow, int* vit, int bonusDamage, int* damageP) {
	int damage;
	if (bonusDamage == 1) {
		damage = (*epow - *vit);
		damage = damage + (damage * .5);
	}
	else if (bonusDamage == 0) {
		damage = (*epow - *vit);
	}

	*hp = *hp - damage;
	*damageP = damage;
}
int main()
{
	string name;
	int role, hp, pow, vit, agi, dex;
	int erole;
	int ehp = 50;
	int epow = 5;
	int evit = 2;
	int eagi = 9;
	int edex = 2;
	int multi = 1;
	int hpHeal = 0;
	string type;
	int damage, hitRate, bonusDamage, ebonusDamage;
	cout << "Tell me your name player: ";
	cin >> name;

	system("CLS");

	cout << "Pick a role: " << endl;
	cout << "[1] Warrior" << endl << "[2] Assasin" << endl << "[3] Mage" << endl;
	cin >> role;
	
	
		if (role == 1) {
		Character* warrior = new Character("Warrior", 100, 9, 1, 9, 6);
		type = "Warrior";
		hp = 100;
		pow = 9;
		vit = 1;
		agi = 9;
		dex = 6;
		}
		else if (role == 2) {
			Character* assassin = new Character("Assassin", 100, 5, 1, 9, 3);
			type = "Assassin";
			hp = 100;
			pow = 5;
			vit = 1;
			agi = 9;
			dex = 3;
		}
		else if (role == 3) {
			Character* mage = new Character("Mage", 100, 5, 2, 7, 2);
			type = "Mage";
			hp = 100;
			pow = 5;
			vit = 2;
			agi = 7;
			dex = 2;
		}
		system("CLS");

		printStats(name, type, hp, pow, vit, agi, dex);
		system("pause");
		system("cls");

		do
		{
			srand(time(NULL));
			int enemyRole;
			enemyRole = rand() % 3 + 1;
			printStats(name, type, hp, pow, vit, agi, dex);
			cout << endl;

			if (enemyRole == 1) {
				cout << "A wild Warrior appeared!!" << endl;
			}
			else if (enemyRole == 2) { cout << "A wild Assassin appeared!!" << endl; }
			else if (enemyRole == 3) { cout << "A wild Mage appeared!!" << endl; }
			cout << "Hp: " << ehp << endl;
			cout << "Pow: " << epow << endl;
			cout << "Vit: " << evit << endl;
			cout << "Agi: " << eagi << endl;
			cout << "Dex:" << edex << endl;
			system("pause");
			system("cls");

			//battle
			cout << "Get ready player the battle will now start!!";
			system("pause");
			system("cls");

			//warrior vs mage
			if (role == 1 && enemyRole == 3) {
				if (role == 1 && enemyRole == 3) {
					bonusDamage = 0;
					ebonusDamage = 1;
				}
				 if (role == 3 && enemyRole == 1) {
					bonusDamage = 1;
					ebonusDamage = 0;
				}
				 if (agi >= eagi || agi == eagi ) {
					 while (ehp >= 0) {
						 playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
						 cout << "Player dealt " << damage << endl;
						 system("pause");
						 enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
						 cout << "Enemy dealt " << damage << endl;
						 system("pause");
					 }
				 }
				 else {
					 while (ehp >= 0) {
						 
					  enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
						 cout << "Enemy dealt " << damage << endl;
						 system("pause");
						 playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
						 cout << "Player dealt " << damage << endl;
						 system("pause");
					 }
				 }
			system("CLS");
			if (hp <= 0) {
				cout << "You lost! :(" << endl;
				printStats(name, type, hp, pow, vit, agi, dex);
				system("pause");
			
			}
			else if (ehp <= 0) {
				cout << "You won! :)" << endl;
				cout << "You gained +5 power! " << endl;
				system("pause");
				ehp = 50 + (5 * multi);
				epow = 5 + (1 * multi);
				evit = 2 + (1 * multi);
				eagi = 9 + (1 * multi);
				edex = 2 + (1 * multi);
				hpHeal = hp;
				hpHeal = hpHeal * .3;
				hp = hp + hpHeal;
				pow = pow + 5;
			}
		}
			//mage vs assassin
			if (role == 3 && enemyRole == 2) {
				if (role == 3 && enemyRole == 2) {
					bonusDamage = 0;
					ebonusDamage = 1;
				}
				if (role == 2 && enemyRole == 3) {
					bonusDamage = 1;
					ebonusDamage = 0;
				}
				if (agi >= eagi || agi == eagi) {
					while (ehp >= 0) {
						playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
						cout << "Player dealt " << damage << endl;
						system("pause");
						enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
						cout << "Enemy dealt " << damage << endl;
						system("pause");
					}
				}else {
					while (ehp >= 0) {

						enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
						cout << "Enemy dealt " << damage << endl;
						system("pause");
						playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
						cout << "Player dealt " << damage << endl;
						system("pause");
					}
				}
				system("CLS");
				if (hp <= 0) {
					cout << "You lost! :(" << endl;
					printStats(name, type, hp, pow, vit, agi, dex);
					system("pause");

				}
				else if (ehp <= 0) {
					cout << "You won! :)" << endl;
					cout << "You gained +3 Agi and +3 Dex! " << endl;
					system("pause");
					ehp = 50 + (5 * multi);
					epow = 5 + (1 * multi);
					evit = 2 + (1 * multi);
					eagi = 9 + (1 * multi);
					edex = 2 + (1 * multi);
					hpHeal = hp;
					hpHeal = hpHeal * .3;
					hp = hp + hpHeal;
					agi = agi + 3;
					dex = dex + 3;
					
				}

			}
			//assassin vs warrior
			if (role == 2 && enemyRole == 1) {
				if (role == 2 && enemyRole == 1) {
					bonusDamage = 0;
					ebonusDamage = 1;
				}
				 if (role == 1 && enemyRole == 2) {
					bonusDamage = 1;
					ebonusDamage = 0;
				}
				 if (agi >= eagi || agi == eagi) {
					 while (ehp >= 0) {
						 playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
						 cout << "Player dealt " << damage << endl;
						 system("pause");
						 enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
						 cout << "Enemy dealt " << damage << endl;
						 system("pause");
					 }
				 }
				 else {
					 while (ehp >= 0) {

						 enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
						 cout << "Enemy dealt " << damage << endl;
						 system("pause");
						 playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
						 cout << "Player dealt " << damage << endl;
						 system("pause");
					 }
				 }
				system("CLS");
				if (hp <= 0) {
					cout << "You lost! :(" << endl;
					printStats(name, type, hp, pow, vit, agi, dex);
					system("pause");

				}
				else if (ehp <= 0) {
					cout << "You won! :)" << endl;
					cout << "You gained +3 Hp and +3 Vit! " << endl;
					system("pause");
					ehp = 50 + (5 * multi);
					epow = 5 + (1 * multi);
					evit = 2 + (1 * multi);
					eagi = 9 + (1 * multi);
					edex = 2 + (1 * multi);
					hpHeal = hp;
					hpHeal = hpHeal * .3;
					hp = hp + hpHeal;
					hp = hp + 3;
					vit = vit + 3;
					
				}
			}
			// same role
			if (role == enemyRole) {
				bonusDamage = 0;
				ebonusDamage = 0;
				while (ehp >= 0) {
					playerAttack(&ehp, &pow, &evit, bonusDamage, &damage);
					cout << "Player dealt " << damage << endl;
					system("pause");
					enemyAttack(&hp, &epow, &vit, ebonusDamage, &damage);
					cout << "Enemy dealt " << damage << endl;
					system("pause");
				}
				system("CLS");
				if (hp <= 0) {
					cout << "You lost! :(" << endl;
					printStats(name, type, hp, pow, vit, agi, dex);
					system("pause");

				}
				else if (ehp <= 0) {
					cout << "You won! :)" << endl;
					system("pause");
					ehp = 50 + (5 * multi);
					epow = 5 + (1 * multi);
					evit = 2 + (1 * multi);
					eagi = 9 + (1 * multi);
					edex = 2 + (1 * multi);
					hpHeal = hp;
					hpHeal = hpHeal * .3;
					hp = hp + hpHeal;
					
				}
			}
			if (hp <= 0) { hp = 0; cout << "Player Hp 0 You lost :<..." << endl; break; }
	} while (hp >= 0);
	system("pause");
}

