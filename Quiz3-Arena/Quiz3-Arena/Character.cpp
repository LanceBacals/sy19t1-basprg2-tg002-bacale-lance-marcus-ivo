#include "Character.h"

Character::Character(string role, int hp, int pow, int vit, int agi, int dex)
{
	mrole = role;
	mhp = hp;
	mpow = pow;
	mvit = vit;
	magi = agi;
	mdex = dex;
}

Character::~Character()
{
}

string Character::getRole()
{
	return mrole;
}

void Character::setRole(string role)
{
	mrole = role;
}

void Character::takeDamage(int damage)
{
	if (damage < 1) return;

	mhp -= damage;
}
