#pragma once
#include <string>

using namespace std;
class Character
{
public:
	Character(string role, int hp, int pow, int vit, int agi, int dex);
	~Character();

	string getRole();
	void setRole(string role);

	void takeDamage(int damage);
private:
	string mrole;
	int mhp;
	int mpow;
	int mvit;
	int magi;
	int mdex;
};

