#include "Unit.h"

Unit::Unit()
{
}

Unit::~Unit()
{
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getAgi()
{
	return mAgi;
}

void Unit::heal(int Hp)
{
	mHp = mHp + 10;
}

void Unit::might(int Pow)
{
	 mPow = mPow + 2;
}

int Unit::iron(int Vit)
{
	mVit = mVit + 2;
}

int Unit::conc(int Dex)
{
	mDex = mDex + 2;
}

int Unit::haste(int Agi)
{
	mAgi = mAgi + 2;
}
