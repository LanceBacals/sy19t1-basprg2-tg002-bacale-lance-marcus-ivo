#pragma once
class Unit
{
public:

	Unit();
	~Unit();

	int getHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();

	void heal(int Hp);
	void might(int Pow);
	void iron(int Vit);
	void conc(int Dex);
	void haste(int Agi);

private:

	int mHp;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;
};

