#include "Item.h"

Item::Item(std::string name)
{
	mName = name;
}

Item::~Item()
{
}

string Item::getName()
{
	return mName;
}

Unit* Item::getActor()
{
	return mActor;
}

void Item::setActor(Unit* actor)
{
	mActor = actor;
}

void Item::activate(Unit* target)
{

	target->getName();
}

