#pragma once
#include <string>
#include <iostream>
#include "Stats.h"
#include <vector>
#include <cstdlib>
#include "Item.h"
using namespace std;
class Item;
class Unit
{
public:
	Unit(string name, const Stats& stats);
	~Unit();

	string getName();
	Stats& getStats();
	int getCurrentHp();
	int getCurrentCrystal();
	int getCurrentRarity();

	const vector<Item*>& getItem();
	void addItem(Item* item);

	
	
	void displayStatus();
	
private:
	string mName;
	Stats mStats;
	int mCurrentHp = 100;
	vector<Item*> mItem;
};

