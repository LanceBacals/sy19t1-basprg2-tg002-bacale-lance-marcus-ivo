#pragma once
#include "Item.h"
#include "Stats.h"
class RarityUp :
	public Item
{
public:
	RarityUp(string name, Unit* actor, StatType type, int bonus);
	~RarityUp();

	int getBonus();
	StatType getType();

	void activate(Unit* target);
	

private:
	int mBonus;
	StatType mType;
};

