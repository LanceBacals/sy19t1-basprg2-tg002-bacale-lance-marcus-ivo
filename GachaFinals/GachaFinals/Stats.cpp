#include "Stats.h"

void addStats(Stats& source, const Stats& toAdd)
{
	source.hp += toAdd.hp;
	source.crystal += toAdd.crystal;
	source.rarity += toAdd.rarity;
}

