#pragma once
#include <iostream>
#include "Unit.h"
#include <string>
class Unit;

using namespace std;

class Item
{
public: 
	Item(string name);
	~Item();

	string getName();
	Unit* getActor();
	void setActor(Unit* actor);
	
	virtual void activate(Unit* target);

private:
	string mName;
	Unit* mActor;
};

