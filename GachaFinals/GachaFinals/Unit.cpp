
#include "Unit.h"

Unit::Unit(string name, const Stats& stats)
{
	mName = name;
	mStats = stats;
}

Unit::~Unit()
{
	for (int i = 0; i < mItem.size(); i++) {
		delete mItem[i];
	}
	mItem.clear();
}

string Unit::getName()
{
	return mName;
}

Stats & Unit::getStats()
{
	return mStats;
}

int Unit::getCurrentHp()
{
	return mStats.hp;
}

int Unit::getCurrentCrystal()
{
	return mStats.crystal;
}

int Unit::getCurrentRarity()
{
	return mStats.rarity;
}

const vector<Item*>& Unit::getItem()
{
	return mItem;
}

void Unit::addItem(Item* item)
{
	item->setActor(this);
	mItem.push_back(item);
}



void Unit::displayStatus()
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mStats.hp << endl;
	cout << "Crystal: " << mStats.crystal << endl;
	cout << "Rarity: " << mStats.rarity << endl;
}
