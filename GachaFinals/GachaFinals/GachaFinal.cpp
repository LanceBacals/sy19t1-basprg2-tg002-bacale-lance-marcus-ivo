#include "Unit.h"
#include "RarityUp.h"
#include <time.h>
#include "Item.h"
Unit* createUnit();

int main()
{
	srand(time(0));
	
	
	Unit* unit = createUnit();
	bool dead = true;
	do
	{
		

		// Show Stats
		unit->displayStatus();
		cout << endl;
		cout << "Each gacha pull will cost you 5 crystals. If you want to continue ";
		system("pause");
		
		cout << endl;
		
		// Pull
		int itemIndex = 0;
		int chance;
		chance = rand() % 101 + 1;
		if (chance == 1) { itemIndex = 0; }
		else if (chance >= 2 && chance <= 11) { itemIndex = 1; }
		else if (chance > 11 && chance <= 51) { itemIndex = 2; }
		else if (chance > 51 && chance <= 66) { itemIndex = 3; }
		else if (chance > 66 && chance <= 86) { itemIndex = 4; }
		else if (chance > 86 && chance <= 100) { itemIndex = 5; }

		if (itemIndex == 0) {
			cout << "You got an SSR!! nicenicenicenice!" << endl;
			Item* item = unit->getItem()[itemIndex];
			item->activate(unit);

			system("pause");
			system("cls");
		}
		if (itemIndex == 1) {
			cout << "You got an SR! congratulations!!" << endl;
			Item* item = unit->getItem()[itemIndex];
			item->activate(unit);

			system("pause");
			system("cls");
		}
		if (itemIndex == 2) {
			cout << "Hey its an R! Not that much but hey thats 1 point away from winning!hekhek" << endl;
			Item* item = unit->getItem()[itemIndex];
			item->activate(unit);

			system("pause");
			system("cls");
		}
		if (itemIndex == 3) {
			cout << "Its a Health Potion! Drink it cuz you need it dumbass! " << endl;
			Item* item = unit->getItem()[itemIndex];
			item->activate(unit);

			system("pause");
			system("cls");
		}
		if (itemIndex == 4) {
			cout << "BOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOM!!!" << endl;
			Item* item = unit->getItem()[itemIndex];
			item->activate(unit);

			system("pause");
			system("cls");
		}
		if (itemIndex == 5) {
			cout << "Hey its crystals! Look at that more crystal to spend!!" << endl;
			Item* item = unit->getItem()[itemIndex];
			item->activate(unit);

			system("pause");
			system("cls");
		}
		if (unit->getCurrentHp() < 0 || unit->getCurrentCrystal() < 0 || unit->getCurrentRarity() >= 100) {
			dead = false;
		}

	} while (dead == true);
	// dead or alive
	if (unit->getCurrentCrystal() < 0) {
		system("cls");
		unit->displayStatus();
		cout << endl << "Nah my dude you broke now :((" << endl;
		system("pause");
	}
	if (unit->getCurrentHp() < 0) {
		system("cls");
		unit->displayStatus();
		cout << endl << "Nah my dude you dead now you pulled boom boom to many times :((" << endl;
		system("pause");
	}
	if (unit->getCurrentRarity() >= 100) {
		system("cls");
		unit->displayStatus();
		cout << endl << "BROO CHILL YOU WON ALREADY YOU RARE PIECE OF METAL NOW CONGRATS!!!" << endl;
		system("pause");
	}
	return 0;
}

Unit* createUnit()
{
	// Create stats
	Stats stats;
	stats.hp = 100;
	stats.crystal = 100;
	stats.rarity = 0;

	// Create Unit
	Unit* unit = new Unit("Ryujin", stats);

	// Create Item
	Item* SSR = new RarityUp("SSR", unit, StatRarity, 50);
	unit->addItem(SSR);
	Item* SR = new RarityUp("SR", unit, StatRarity, 10);
	unit->addItem(SR);
	Item* R = new RarityUp("R", unit, StatRarity, 1);
	unit->addItem(R);
	Item* HealthPotion = new RarityUp("Health Potion", unit, StatHp, 30);
	unit->addItem(HealthPotion);
	Item* Bomb = new RarityUp("Bomb", unit, StatHp, -100);
	unit->addItem(Bomb);
	Item* Crystal = new RarityUp("Crystal", unit, StatCrystal, 15);
	unit->addItem(Crystal);

	return unit;
}
