#include "RarityUp.h"
#include "Unit.h"
RarityUp::RarityUp(string name, Unit* actor, StatType type, int bonus)
	:Item(name)
{
	mType = type;
	mBonus = bonus;
}

RarityUp::~RarityUp()
{
}

int RarityUp::getBonus()
{
	return mBonus;
}

StatType RarityUp::getType()
{
	return mType;
}

void RarityUp::activate(Unit* target)
{
	target->getStats().crystal -= 5;
	string statName;
	switch (mType)
	{
	case StatHp:
		statName = "Hp";
		target->getStats().hp += mBonus;
		break;

	case StatCrystal:
		statName = "Crystal";
		target->getStats().crystal += mBonus;
		break;
		
	case StatRarity:
		statName = "Rarity";
		target->getStats().rarity += mBonus;
		break;
	default:
		break;
	}
	if (mBonus != -25) {
		cout << target->getName() << "'s " << statName << " increased!" << endl;
		cout << statName << " + " << mBonus << "!" << endl;
	}else cout << target->getName() << "'s " << statName << " decreased by 25" << endl;

}
