#pragma once
#include <iostream>
#include <string>
using namespace std;

enum StatType {
	StatCrystal,
	StatHp,
	StatRarity
};

struct Stats
{
	int hp = 0;
	int crystal = 0;
	int rarity = 0;

};

void addStats(Stats& source, const Stats& toAdd);

