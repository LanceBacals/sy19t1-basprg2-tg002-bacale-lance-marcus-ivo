#pragma once
#include <string>
#include "Wizard.h"

using namespace std;
class Skill
{
public:
	Skill(string name, int mpCost);
	

	string getName();
	int getMpCost();
	void fight();

private:
	string mName;
	int mMpCost;
};

