#pragma once
#include <string>
#include "Skill.h"

using namespace std;

class Wizard
{
public:
	Wizard();

	string getWizard();
	int getHp();
	int getMp();

private:
	string mWizard;
	int mHp;
	int mMp;

};

