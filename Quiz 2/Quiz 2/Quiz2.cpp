#include <iostream>
#include <string>
#include <time.h>

#ifndef NODE_H
#define NODE_H
using namespace std;



	struct Node
	{
		std::string name;
		string soldier;
		Node* next = NULL;
		Node* previous = NULL;
	};

#endif 
	void deletion(Node* head) {
		if (head != NULL) {
			delete head;
			head = NULL;
		}
	}
	void display(Node* head) { // display function
		Node* ptr = head;
		while (ptr != NULL) {
			cout << ptr->soldier << endl;
			ptr = ptr->next;
		}
}
int main() {
	srand(time(NULL));
	string soldier1, soldier2, soldier3, soldier4, soldier5;

	cout << "Enter a name: ";
	cin >> soldier1;
	cout << endl << "Enter a name: ";
	cin >> soldier2;
	cout << endl << "Enter a name: ";
	cin >> soldier3;
	cout << endl << "Enter a name: ";
	cin >> soldier4;
	cout << endl << "Enter a name: ";
	cin >> soldier5;

	Node* node1 = new Node;
	node1->soldier = soldier1;

	Node* node2 = new Node;
	node2->soldier = soldier2;
	node1->next = node2;

	Node* node3 = new Node;
	node3->soldier = soldier3;
	node2->next = node3;

	Node* node4 = new Node;
	node4->soldier = soldier4;
	node3->next = node4;

	Node* node5 = new Node;
	node5->soldier = soldier5;
	node4->next = node5;

	int cloak = 0;
	int numPlayers = 4;
	
	while (numPlayers != 0) {

		cloak = rand() % numPlayers + 1;
			Node* temp = new Node;
			Node* last = new Node;
			int counter = 0;
		//Lucky Roll 

			if (cloak == 1) {
				if (node2 != NULL) {
					node1->next = node2->next;
					delete node2;
					node2 = NULL;
				}
				
					display(node1);
				numPlayers = numPlayers - 1;
			
		}
		 if (cloak == 2) {
			if (node3 != NULL) {
				node1->next = node3->next;
				delete node3;
				node3 = NULL;
			}
			display(node1);
			numPlayers = numPlayers - 1;
		}
		if (cloak == 3) {
			if (node4 != NULL) {
				node1->next = node4->next;
				delete node4;
				node4 = NULL;
			}
			display(node1);
			numPlayers = numPlayers - 1;
		}
		 if (cloak == 4) {
			if (node5 != NULL) {
				delete node5;
				node5 = NULL;
			}
			display(node1);
			numPlayers = numPlayers - 1;
		}
		cout << endl << "=============================================================" << endl;
		system("pause");
	}
	if (numPlayers == 0) {
		cout << "Soldier " << node1->soldier << " is the chosen one!" << endl;
	}
		display(node1);

	return 0;

}
