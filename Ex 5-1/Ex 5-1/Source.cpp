#include <iostream>
using namespace std;
class Shape {
public:
	int mWidth, mHeight;
	void setWandH(int width, int height) {
		mWidth = width;
		mHeight = height;
	}

};
class Square: public Shape {
public:
	int area() {
		return mWidth * mWidth;
	}
};
class Rectangle : public Shape {
public:
	int area()
	{
		return mWidth * mHeight;
	}
};

class Circle : public Shape {
public:
	float area()
	{
		float pi = 3.1415926535897;
		return pi * (mWidth * mWidth);
	}
};
int main() {

	Rectangle rect;
	Circle circ;
	Square sqr;
	Shape* rectangle = &rect;
	Shape* circle = &circ;
	Shape* square = &sqr;
	rectangle->setWandH(10 , 20);
	circle->setWandH(50 , 0);
	square->setWandH(4 , 0);
	cout << "Rectangle" << endl << "Num of sides = 4 " << endl;
	cout << "Area: " << rect.area() << endl;
	cout << "Circle" << endl << "Num of sides = 0 " << endl;
	cout << "Area: " << circ.area() << endl;
	cout << "Square" << endl << "Num of sides = 4 " << endl;
	cout << "Area: " << sqr.area() << endl;
	return 0;
}
