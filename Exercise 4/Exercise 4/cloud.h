

#include <iostream>
#include <string>

using namespace std;

struct character
{
	string name;
	int lvl, exp, hp, mp;
	int str, dex, vit, mag, spirit, luck;
	int atk, atkPer, def, defPer, magAtk, magDef, magDefPer;
};

void printCloudStats(character cloud)
{
	cout << "Name: " << cloud.name;
	cout << "              EXP: " << cloud.exp << "p" << endl;
	cout << "LV: " << cloud.lvl << endl;
	cout << "HP: " << cloud.hp << endl;
	cout << "MP: " << cloud.mp << endl;
	cout << "Strength: " << cloud.str << endl;
	cout << "Dexterity: " << cloud.dex << endl;
	cout << "Vitality: " << cloud.vit << endl;
	cout << "Magic: " << cloud.mag << endl;
	cout << "Spirit: " << cloud.spirit << endl;
	cout << "Luck: " << cloud.luck << endl << endl;
	cout << "Attack: " << cloud.atk << endl;
	cout << "Attack%: " << cloud.atkPer << endl;
	cout << "Defense: " << cloud.def << endl;
	cout << "Defense%: " << cloud.defPer << endl;
	cout << "Magic Attack: " << cloud.magAtk << endl;
	cout << "Magic Defense: " << cloud.magDef << endl;
	cout << "Magic Defense%: " << cloud.magDefPer << endl;
}

int main()
{
	character cloud;

	cloud.name = "Cloud";
	cloud.exp = 5944665;
	cloud.hp = 8759;
	cloud.mp = 920;
	cloud.str = 253;
	cloud.dex = 255;
	cloud.vit = 143;
	cloud.mag = 173;
	cloud.spirit = 181;
	cloud.luck = 197;
	cloud.atk = 255;
	cloud.atkPer = 110;
	cloud.def = 149;
	cloud.defPer = 66;
	cloud.magAtk = 173;
	cloud.magDef = 181;
	cloud.magDefPer = 3;

	printCloudStats(cloud);
}
